#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Helper.h"
#include <Windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>


using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

typedef unsigned int (WINAPI* AvVersion)(void);

int main()
{
	Helper helper;
	string command;
	vector<string> words;
	char* path = new char[MAX_PATH];


	LARGE_INTEGER filesize;
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	TCHAR szDir[MAX_PATH];

	HANDLE h;
	
	GetModuleFileNameA(NULL, path, MAX_PATH);
	while (true)
	{
		
		cout << ">> ";
		std::getline(cin, command);
		helper.trim(command);
		words = helper.get_words(command);

		if (words[0].compare("pwd") == 0)
		{
			cout << path << endl;
		}
		else if (words[0].compare("cd") == 0)
		{
			if (words[1].compare("..") == 0) 
			{
				string pathTemp;
				int slashLastIndex = string(path).find_last_of("\\");
				if (slashLastIndex != string::npos) 
				{
					pathTemp = (string(path)).substr(0, slashLastIndex);
					SetCurrentDirectoryA(pathTemp.c_str());
					strcpy(path, pathTemp.c_str());
				}
				else
				{
					cout << "Could not go back. There is no \\ in the current path" << endl;
				}
			}
			else
			{
				path = _strdup(words[1].c_str());
				cout << path << endl;
			}
		}
		else if (words[0].compare("ls") == 0)
		{
			StringCchCopy(szDir, MAX_PATH, path);
			StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

			hFind = FindFirstFile(szDir, &ffd);


			do
			{
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
				}
				else
				{
					filesize.LowPart = ffd.nFileSizeLow;
					filesize.HighPart = ffd.nFileSizeHigh;
					_tprintf(TEXT("  %s   \n"), ffd.cFileName);
				}
			} while (FindNextFile(hFind, &ffd) != 0);
		}
		else if (words[0].compare("create") == 0)
		{
			h = CreateFile(words[1].c_str(), GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

			if (h)
			{
				std::cout << "CreateFile() succeeded\n";
				CloseHandle(h);
			}
			else
			{
				std::cerr << "CreateFile() failed:" << GetLastError() << "\n";
			}
		}
		else if (words[0].compare("secret") == 0)
		{
			HMODULE dll = LoadLibraryA("SECRET.dll");
			if (dll != NULL)
			{
				AvVersion func = (AvVersion)GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything");
				if (func != NULL)
				{
					int result = func();
					cout << result << endl;
				}
				else
				{
					cout << "Cant open the function" << endl;
				}
			}
			else
			{
				cout << "Cant open the dll" << endl;
			}
		}
		else
		{
			LPCSTR temp = LPCSTR(words[0].c_str());

			WinExec(temp, SW_SHOWNORMAL);


		}
	}

	return 0;
}